package org.manong.diary.plan.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import org.manong.diary.plan.PlanApplication;
import org.manong.diary.plan.R;
import org.manong.diary.plan.bean.Item;
import org.manong.diary.plan.utils.DateUtil;
import org.manong.diary.plan.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by YangXiufeng on 2015/12/16.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private static final String TAG = "RecyclerAdapter";
    private ArrayList<Item> itemList = new ArrayList<>();
    public RecyclerAdapter(ArrayList<Item> items){
        itemList.addAll(items);
    }
    public void addItem(Item item){
        itemList.add(item);
        notifyDataSetChanged();
    }
    public void addItems(ArrayList<Item> items){
        itemList.addAll(items);
        notifyDataSetChanged();
    }
    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item item = itemList.get(position);
        Log.e("xx", item.getmToDoText());
        holder.mToDoTextview.setText(item.getmToDoText());
        TextDrawable myDrawable = TextDrawable.builder().beginConfig()
                .textColor(Color.WHITE)
                .useFont(Typeface.DEFAULT)
                .toUpperCase()
                .endConfig()
                .buildRound(item.getmToDoText().substring(0,1),item.getmTodoColor());
        holder.mColorImageView.setImageDrawable(myDrawable);
        if(item.getmToDoDate()!=null){
            String timeToShow;
            if(android.text.format.DateFormat.is24HourFormat(PlanApplication.getInstance())){
                timeToShow = DateUtil.formatDate(DateUtil.DATE_TIME_FORMAT_24_HOUR, item.getmToDoDate());
            }
            else{
                timeToShow = DateUtil.formatDate(DateUtil.DATE_TIME_FORMAT_12_HOUR, item.getmToDoDate());
            }
            holder.mTimeTextView.setText(timeToShow);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.listItemLinearLayout)
        LinearLayout linearLayout;
        @Bind(R.id.toDoListItemTextview)
        TextView mToDoTextview;
        @Bind(R.id.toDoListItemColorImageView)
        ImageView mColorImageView;
        @Bind(R.id.todoListItemTimeTextView)
        TextView mTimeTextView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }
    }
}
