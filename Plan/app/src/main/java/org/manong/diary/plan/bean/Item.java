package org.manong.diary.plan.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Created by YangXiufeng on 2015/12/16.
 */
public class Item implements Serializable{

    private static final String TAG = "Item";
    private static final long serialVersionUID = 6023417811422330998L;
    private String mToDoText;
    private int mHasReminder;
    //    private Date mLastEdited;
    private int mTodoColor;
    private Date mToDoDate;
    private UUID mTodoIdentifier;
    public Item(){}
    public Item(String mToDoText, int mHasReminder, int mTodoColor, Date mToDoDate, UUID mTodoIdentifier) {
        this.mToDoText = mToDoText;
        this.mHasReminder = mHasReminder;
        this.mTodoColor = mTodoColor;
        this.mToDoDate = mToDoDate;
        this.mTodoIdentifier = mTodoIdentifier;
    }

    public UUID getmTodoIdentifier() {
        return mTodoIdentifier;
    }

    public void setmTodoIdentifier(UUID mTodoIdentifier) {
        this.mTodoIdentifier = mTodoIdentifier;
    }

    public Date getmToDoDate() {
        return mToDoDate;
    }

    public void setmToDoDate(Date mToDoDate) {
        this.mToDoDate = mToDoDate;
    }

    public int getmTodoColor() {
        return mTodoColor;
    }

    public void setmTodoColor(int mTodoColor) {
        this.mTodoColor = mTodoColor;
    }

    public int getmHasReminder() {
        return mHasReminder;
    }

    public void setmHasReminder(int mHasReminder) {
        this.mHasReminder = mHasReminder;
    }

    public String getmToDoText() {
        return mToDoText;
    }

    public void setmToDoText(String mToDoText) {
        this.mToDoText = mToDoText;
    }



    @Override
    public String toString() {
        return "Item{" +
                "mToDoText='" + mToDoText + '\'' +
                ", mHasReminder=" + mHasReminder +
                ", mTodoColor=" + mTodoColor +
                ", mToDoDate=" + mToDoDate +
                ", mTodoIdentifier=" + mTodoIdentifier +
                '}';
    }
}
