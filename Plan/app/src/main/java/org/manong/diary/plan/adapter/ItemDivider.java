package org.manong.diary.plan.adapter;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;

/**
 * Created by YangXiufeng on 2015/12/16.
 */
public class ItemDivider extends RecyclerView.ItemDecoration{
    private static final String TAG = "ItemDivider";

    public ItemDivider() {
        super();
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
    }
}
