package org.manong.diary.plan.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by YangXiufeng on 2015/12/16.
 */
public class DateUtil {
    private static final String TAG = "DateUtil";
    public static final String DATE_TIME_FORMAT_12_HOUR = "MMM d, yyyy  h:mm a";
    public static final String DATE_TIME_FORMAT_24_HOUR = "MMM d, yyyy  k:mm";
    public static String formatDate(String formatString, Date dateToFormat){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatString);
        return simpleDateFormat.format(dateToFormat);
    }
}
