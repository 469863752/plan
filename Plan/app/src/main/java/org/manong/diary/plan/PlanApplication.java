package org.manong.diary.plan;

import android.app.Application;

/**
 * Created by YangXiufeng on 2015/12/16.
 */
public class PlanApplication extends Application{
    private static final String TAG = "PlanApplication";
    private static PlanApplication instance;

    public static PlanApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
